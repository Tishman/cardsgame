//
//  EmojiMemoryGame.swift
//  CardsGame
//
//  Created by Роман Тищенко on 14.03.2021.
//

import Foundation

final class EmojiMemoryGame {
    private var model: MemoryGame<String>
    
    // MARK: - Access to the Model
    
    var cards: Array<MemoryGame<String>.Card> {
        model.cards
    }
    
    // MARK: - Intent(s)
    
    func choose(card: MemoryGame<String>.Card) {
        model.choose(card: card)
    }
}
